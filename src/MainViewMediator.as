/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 10:41
 * The Media Workshop Ltd
 */
package {
    import org.robotlegs.mvcs.Mediator;

    import tmw.arabianmaps.controller.events.TileLoaderEvent;
    import tmw.arabianmaps.model.ITileModel;

    public class MainViewMediator extends Mediator
    {
        [Inject]
        public var view:MainView;

        [Inject]
        public var tileModel:ITileModel;

        override public function onRegister():void
        {
            addContextListener(TileLoaderEvent.COMPLETE, evtTilesLoaded);
        }

        private function evtTilesLoaded(event:TileLoaderEvent):void
        {
            removeContextListener(TileLoaderEvent.COMPLETE, evtTilesLoaded);
            view.init();
        }
    }
}
