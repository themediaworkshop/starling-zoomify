/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 10:35
 * The Media Workshop Ltd
 */
package {
    import com.furusystems.dconsole2.DConsole;

    import tmw.arabianmaps.ArabianMaps;

    import flash.display.Sprite;
    import flash.geom.Point;

    import tmw.arabianmaps.controller.events.MapEvent;

    import tmw.text.SimpleTextField;
    import tmw.text.Text;
    import tmw.text.TextFactory;

    public class MainView extends Sprite
    {
        private var maps:ArabianMaps;
        private var text:SimpleTextField;

        public function MainView()
        {
            maps = new ArabianMaps();
            addChild(maps);
        }

        public function init():void
        {
            text = new SimpleTextField(TextFactory.createTextXML("Hello!", Text.LANGUAGE_ENGLISH, NaN,NaN,null,20,"#FF0000"));
            addChild(text);

            maps.addEventListener(MapEvent.ANCHOR_UPDATE, evtPan);

            DConsole.createCommand("tween", maps.tweenTo);
        }

        private function evtPan(event:MapEvent):void
        {
            var point:Point = maps.mapToStagePoint(new Point(4000,2500));
            text.x = point.x;
            text.y = point.y;
        }
    }
}
