package {

    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;

    public class StarlingZoomifyMain extends Sprite {

        protected var _context:StarlingZoomifyContext;

        public function StarlingZoomifyMain() {
            stage.frameRate = 60;
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;

            _context = new StarlingZoomifyContext(this);
        }
    }
}