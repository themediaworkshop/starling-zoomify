/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 29/10/2013
 * Time: 12:50
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.events
{
    import flash.events.Event;

    public class WindowSetupEvent extends Event
    {

        public static const DO_SETUP:String = "WindowSetupEvent.doSetup";
        public static const COMPLETE:String = "WindowSetupEvent.complete";

        private var _windows:XMLList;

        public function WindowSetupEvent(type:String, windows:XMLList = null)
        {
            _windows = windows;
            super(type);
        }

        override public function clone():Event
        {
            return new WindowSetupEvent(type, windows);
        }

        public function get windows():XMLList
        {
            return _windows;
        }
    }
}
