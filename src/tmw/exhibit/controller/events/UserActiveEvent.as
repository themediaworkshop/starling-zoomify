/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 05/02/2014
 * Time: 10:36
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.events
{
    import flash.events.Event;

    public class UserActiveEvent extends Event
    {

        public static const USER_ACTIVE:String = "UserActiveEvent.userActive";
        public static const PAUSE_TIMERS:String = "UserActiveEvent.pauseTimers";
        public static const START_TIMERS:String = "UserActiveEvent.startTimers";

        public function UserActiveEvent(type:String)
        {
            super(type);
        }

        override public function clone():Event
        {
            return new UserActiveEvent(type);
        }
    }
}
