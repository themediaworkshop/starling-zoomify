/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 24/10/2013
 * Time: 16:50
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.events
{
    import flash.events.Event;

    public class ChangeLanguageEvent extends Event
    {

        public static const CHANGE_TO_ENGLISH:String = "ChangeLanguageEvent.changeToEnglish";
        public static const CHANGE_TO_ARABIC:String = "ChangeLanguageEvent.changeToArabic";

        public function ChangeLanguageEvent(type:String)
        {
            super(type);
        }

        override public function clone():Event
        {
            return new ChangeLanguageEvent(type);
        }
    }
}
