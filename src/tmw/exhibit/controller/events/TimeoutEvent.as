/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 05/02/2014
 * Time: 10:32
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.events
{
    import flash.events.Event;

    public class TimeoutEvent extends Event
    {

        public static const SHOW_WARNING:String = "TimeoutEvent.showWarning";
        public static const COUNTDOWN:String = "TimeoutEvent.countdown";

        private var _count:int;

        public function TimeoutEvent(type:String, count:int = NaN)
        {
            _count = count;

            super(type);
        }

        override public function clone():Event
        {
            return new TimeoutEvent(type, count);
        }

        public function get count():int
        {
            return _count;
        }

    }
}
