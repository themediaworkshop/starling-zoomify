/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 22/10/2013
 * Time: 11:32
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.events
{
    import flash.events.Event;

    public class ContentLoadEvent extends Event
    {

        public static const COMPLETE:String = "ContentLoadEvent.complete";
        public static const LOAD:String = "ContentLoadEvent.load";

        private var _data:XML;

        public function ContentLoadEvent(type:String, data:XML = null)
        {
            _data = data;
            super(type);
        }

        override public function clone():Event
        {
            return new ContentLoadEvent(type, data);
        }

        public function get data():XML
        {
            return _data;
        }
    }
}
