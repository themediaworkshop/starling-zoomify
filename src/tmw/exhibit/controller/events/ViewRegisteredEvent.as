/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 07/03/2014
 * Time: 12:22
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.events
{
    import flash.events.Event;

    public class ViewRegisteredEvent extends Event
    {

        public static const VIEW_REGISTERED:String = "ViewRegisteredEvent.viewRegistered";

        public function ViewRegisteredEvent(type:String,bubbles:Boolean = false,cancelable:Boolean = false)
        {
            super(type,bubbles,cancelable);
        }

        override public function clone():Event
        {
            return new ViewRegisteredEvent(type,bubbles,cancelable);
        }
    }
}
