/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 22/10/2013
 * Time: 15:51
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.events
{
    import flash.events.Event;

    public class ExhibitEvent extends Event
    {

        public static const INIT:String = "ExhibitEvent.init";

        public function ExhibitEvent(type:String)
        {
            super(type);
        }

        override public function clone():Event
        {
            return new ExhibitEvent(type);
        }
    }
}
