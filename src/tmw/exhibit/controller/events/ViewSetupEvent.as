/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 29/10/2013
 * Time: 13:40
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.events
{
    import flash.events.Event;

    import tmw.exhibit.model.IContentLoader;

    public class ViewSetupEvent extends Event
    {

        public static const SETUP_ROOT_VIEWS:String = "ViewSetupEvent.setupRootViews";
        public static const APPLY_CONFIG:String = "ViewSetupEvent.applyConfig";

        private var _config:Object;
        private var _contentLoader:IContentLoader;

        public function ViewSetupEvent(type:String, config:Object = null, contentLoader:IContentLoader = null)
        {
            _config = config;
            _contentLoader = contentLoader;
            super(type);
        }

        override public function clone():Event
        {
            return new ViewSetupEvent(type, config)
        }

        public function get config():Object
        {
            return _config;
        }

        public function get contentLoader():IContentLoader
        {
            return _contentLoader;
        }
    }
}
