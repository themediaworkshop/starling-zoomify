/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 05/02/2014
 * Time: 10:29
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.events
{
    import flash.events.Event;

    public class RestartEvent extends Event
    {
        public static const RESTART:String = "RestartEvent.restart";

        public function RestartEvent(type:String,bubbles:Boolean = false,cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }

        override public function clone():Event
        {
            return new RestartEvent(type);
        }
    }
}
