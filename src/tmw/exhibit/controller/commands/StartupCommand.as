/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 22/10/2013
 * Time: 11:50
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.commands
{
    import org.robotlegs.mvcs.Command;

    import tmw.exhibit.controller.events.ContentLoadEvent;

    public class StartupCommand extends Command
    {
        override public function execute():void
        {
            dispatch(new ContentLoadEvent(ContentLoadEvent.LOAD));
        }
    }
}
