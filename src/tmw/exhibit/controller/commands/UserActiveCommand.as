/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 05/02/2014
 * Time: 10:36
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.commands
{
    import org.robotlegs.mvcs.Command;

    import tmw.exhibit.controller.events.UserActiveEvent;
    import tmw.exhibit.service.ITimerService;

    public class UserActiveCommand extends Command
    {
        [Inject]
        public var event:UserActiveEvent;

        [Inject]
        public var timerService:ITimerService;

        override public function execute():void
        {
            switch(event.type)
            {
                case UserActiveEvent.START_TIMERS:
                    timerService.resetUserActiveTimer();
                    timerService.resetCancelTimeoutTimer();
                    timerService.startUserActiveTimer();
                    break;
                case UserActiveEvent.PAUSE_TIMERS:
                    timerService.stopUserActiveTimer();
                    timerService.stopCancelTimeoutTimer();
                    timerService.resetUserActiveTimer();
                    timerService.resetCancelTimeoutTimer();
                    break;
                case UserActiveEvent.USER_ACTIVE:
                    timerService.resetUserActiveTimer();
                    timerService.resetCancelTimeoutTimer();
                    if(timerService.isTiming) timerService.startUserActiveTimer();
                    break;
            }
        }
    }
}
