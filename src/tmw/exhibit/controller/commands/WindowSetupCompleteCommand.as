/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 29/10/2013
 * Time: 13:38
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.commands
{
    import org.robotlegs.mvcs.Command;

    import tmw.exhibit.controller.events.ViewSetupEvent;

    import tmw.exhibit.controller.events.ChangeLanguageEvent;

    import tmw.exhibit.controller.events.ExhibitEvent;

    import tmw.exhibit.model.IConfigModel;
    import tmw.exhibit.model.IContentLoader;
    import tmw.exhibit.model.IExhibitStateModel;

    public class WindowSetupCompleteCommand extends Command
    {

        [Inject]
        public var configModel:IConfigModel;

        [Inject]
        public var contentLoader:IContentLoader;

        [Inject]
        public var exhibitState:IExhibitStateModel;

        override public function execute():void
        {
            // add root views
            dispatch(new ViewSetupEvent(ViewSetupEvent.SETUP_ROOT_VIEWS));

            // setup views
            dispatch(new ViewSetupEvent(ViewSetupEvent.APPLY_CONFIG, configModel.data, contentLoader));

            // set load complete flag for future registered views
            exhibitState.loadComplete = true;

            // start exhibit
            dispatch(new ExhibitEvent(ExhibitEvent.INIT));

            //For now set to English
            dispatch(new ChangeLanguageEvent(ChangeLanguageEvent.CHANGE_TO_ENGLISH));
        }
    }
}
