/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 22/10/2013
 * Time: 11:32
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.commands
{
    import org.robotlegs.mvcs.Command;

    import tmw.exhibit.controller.events.ChangeLanguageEvent;

    import tmw.exhibit.controller.events.ContentLoadEvent;
    import tmw.exhibit.controller.events.ExhibitEvent;
    import tmw.exhibit.controller.events.WindowSetupEvent;
    import tmw.exhibit.model.IConfigModel;
    import tmw.exhibit.service.ITimerService;

    public class ContentLoadCompleteCommand extends Command
    {
        [Inject]
        public var event:ContentLoadEvent;

        [Inject]
        public var configModel:IConfigModel;

        [Inject]
        public var timerService:ITimerService;

        override public function execute():void
        {
            // add content to model
            configModel.loadData(event.data);

            // setup timers
            timerService.setupTimers(configModel.data.timers.userActive, configModel.data.timers.cancelTimeoutWait);

            // do window setup
            dispatch(new WindowSetupEvent(WindowSetupEvent.DO_SETUP, configModel.data.window));
        }
    }
}
