/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 29/10/2013
 * Time: 12:52
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.commands
{
    import flash.display.NativeWindow;
    import flash.display.NativeWindowInitOptions;
    import flash.display.NativeWindowSystemChrome;
    import flash.display.NativeWindowType;
    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.geom.Rectangle;

    import org.robotlegs.mvcs.Command;

    import tmw.exhibit.controller.events.WindowSetupEvent;
    import tmw.exhibit.model.IWindowsModel;
    import tmw.exhibit.model.values.WindowVO;

    public class WindowSetupCommand extends Command
    {

        [Inject]
        public var event:WindowSetupEvent;

        [Inject]
        public var windowsModel:IWindowsModel;

        override public function execute():void
        {

            // if no windows defined in XML add primary native window to windows model
            if(event.windows.length() == 0)
            {
                windowsModel.push(new WindowVO(contextView.stage.nativeWindow, contextView as Sprite));
                dispatch(new WindowSetupEvent(WindowSetupEvent.COMPLETE));
                return;
            }

            // create windows
            var windowOptions:NativeWindowInitOptions = new NativeWindowInitOptions();
            windowOptions.systemChrome = NativeWindowSystemChrome.NONE;
            windowOptions.type = NativeWindowType.NORMAL;

            for each(var window:XML in event.windows)
            {
                var newWindow:NativeWindow = new NativeWindow(windowOptions);
                newWindow.stage.scaleMode = StageScaleMode.NO_SCALE;
                newWindow.stage.align = StageAlign.TOP_LEFT;
                newWindow.bounds = new Rectangle(parseFloat(window.@x), parseFloat(window.@y), parseFloat(window.@width), parseFloat(window.@height));
                newWindow.activate();
                var rootView:Sprite = new Sprite();
                newWindow.stage.addChild(rootView);
                windowsModel.push(new WindowVO(newWindow, rootView));
            }

            // close primary native window
            contextView.stage.nativeWindow.close();

            dispatch(new WindowSetupEvent(WindowSetupEvent.COMPLETE));
        }
    }
}
