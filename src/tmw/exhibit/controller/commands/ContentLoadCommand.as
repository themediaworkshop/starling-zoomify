package tmw.exhibit.controller.commands
{
    import tmw.exhibit.model.values.ConfigFilePath;
    import tmw.exhibit.service.IContentLoadingService;

    import org.robotlegs.mvcs.Command;

    public class ContentLoadCommand extends Command
    {
        [Inject]
        public var configFilePath:ConfigFilePath;

        [Inject]
        public var contentLoadingService:IContentLoadingService;

        override public function execute():void
        {
            contentLoadingService.load(configFilePath.toString());
        }
    }
}