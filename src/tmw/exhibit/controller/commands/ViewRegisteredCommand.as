/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 07/03/2014
 * Time: 12:21
 * The Media Workshop Ltd
 */
package tmw.exhibit.controller.commands
{
    import org.robotlegs.mvcs.Command;

    import tmw.exhibit.controller.events.ViewRegisteredEvent;
    import tmw.exhibit.controller.events.ViewSetupEvent;
    import tmw.exhibit.model.IConfigModel;
    import tmw.exhibit.model.IContentLoader;
    import tmw.exhibit.model.IExhibitStateModel;

    public class ViewRegisteredCommand extends Command
    {
        [Inject]
        public var event:ViewRegisteredEvent;

        [Inject]
        public var configModel:IConfigModel;

        [Inject]
        public var contentLoader:IContentLoader;

        [Inject]
        public var exhibitState:IExhibitStateModel;

        override public function execute():void
        {
            // setup views that didn't get setup on init
            if(exhibitState.loadComplete)
                dispatch(new ViewSetupEvent(ViewSetupEvent.APPLY_CONFIG, configModel.data, contentLoader));
        }
    }
}
