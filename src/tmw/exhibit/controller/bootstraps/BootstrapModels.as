package tmw.exhibit.controller.bootstraps
{
    import org.robotlegs.core.IInjector;

    import tmw.exhibit.model.ConfigModel;
    import tmw.exhibit.model.ContentLoader;
    import tmw.exhibit.model.ExhibitStateModel;
    import tmw.exhibit.model.IConfigModel;
    import tmw.exhibit.model.IContentLoader;
    import tmw.exhibit.model.IExhibitStateModel;
    import tmw.exhibit.model.IWindowsModel;
    import tmw.exhibit.model.WindowsModel;

    public class BootstrapModels
    {
        public function BootstrapModels(injector:IInjector)
        {
            injector.mapSingletonOf(IConfigModel, ConfigModel);
            injector.mapSingletonOf(IContentLoader, ContentLoader);
            injector.mapSingletonOf(IWindowsModel, WindowsModel);
            injector.mapSingletonOf(IExhibitStateModel, ExhibitStateModel);
        }
    }
}