package tmw.exhibit.controller.bootstraps
{
    import tmw.exhibit.model.values.ConfigFilePath;

    import org.robotlegs.core.IInjector;

    public class BootstrapConfigValues
    {
        public function BootstrapConfigValues(injector:IInjector)
        {
            injector.mapValue(ConfigFilePath, new ConfigFilePath("content.xml"));
        }
    }
}