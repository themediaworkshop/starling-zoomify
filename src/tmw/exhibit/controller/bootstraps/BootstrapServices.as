package tmw.exhibit.controller.bootstraps
{
    import tmw.exhibit.service.ContentLoadingService;
    import tmw.exhibit.service.IContentLoadingService;

    import org.robotlegs.core.IInjector;

    import tmw.exhibit.service.ITimerService;
    import tmw.exhibit.service.TimerService;

    public class BootstrapServices
    {
        public function BootstrapServices(injector:IInjector)
        {
            injector.mapSingletonOf(IContentLoadingService, ContentLoadingService);
            injector.mapSingletonOf(ITimerService, TimerService);
        }

    }

}