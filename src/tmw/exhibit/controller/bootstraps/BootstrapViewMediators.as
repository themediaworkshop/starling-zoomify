package tmw.exhibit.controller.bootstraps
{
    import tmw.exhibit.model.values.WindowVO;

    import tmw.text.DualLanguageTextField;
    import tmw.text.DualLanguageTextFieldMediator;

    public class BootstrapViewMediators extends Object
    {
        public function BootstrapViewMediators(windows:Vector.<WindowVO>)
        {
            for each(var window:WindowVO in windows)
            {
                window.mediatorMap.mapView(DualLanguageTextField, DualLanguageTextFieldMediator);
            }
        }
    }
}