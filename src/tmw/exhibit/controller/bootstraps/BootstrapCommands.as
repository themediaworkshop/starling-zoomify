package tmw.exhibit.controller.bootstraps
{
    import tmw.exhibit.controller.commands.ContentLoadCommand;

    import org.robotlegs.base.ContextEvent;
    import org.robotlegs.core.ICommandMap;

    import tmw.exhibit.controller.commands.ContentLoadCompleteCommand;

    import tmw.exhibit.controller.commands.StartupCommand;
    import tmw.exhibit.controller.commands.UserActiveCommand;
    import tmw.exhibit.controller.commands.ViewRegisteredCommand;
    import tmw.exhibit.controller.commands.WindowSetupCommand;
    import tmw.exhibit.controller.commands.WindowSetupCompleteCommand;
    import tmw.exhibit.controller.events.ContentLoadEvent;
    import tmw.exhibit.controller.events.UserActiveEvent;
    import tmw.exhibit.controller.events.ViewRegisteredEvent;
    import tmw.exhibit.controller.events.WindowSetupEvent;

    public class BootstrapCommands
    {
        public function BootstrapCommands(commandMap:ICommandMap)
        {
            commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, StartupCommand, ContextEvent, true);
            commandMap.mapEvent(ContentLoadEvent.LOAD, ContentLoadCommand, ContentLoadEvent, true);
            commandMap.mapEvent(ContentLoadEvent.COMPLETE, ContentLoadCompleteCommand, ContentLoadEvent, true);
            commandMap.mapEvent(WindowSetupEvent.DO_SETUP, WindowSetupCommand, WindowSetupEvent, true);
            commandMap.mapEvent(WindowSetupEvent.COMPLETE, WindowSetupCompleteCommand, WindowSetupEvent, true);

            commandMap.mapEvent(ViewRegisteredEvent.VIEW_REGISTERED, ViewRegisteredCommand, ViewRegisteredEvent);

            commandMap.mapEvent(UserActiveEvent.START_TIMERS, UserActiveCommand, UserActiveEvent);
            commandMap.mapEvent(UserActiveEvent.PAUSE_TIMERS, UserActiveCommand, UserActiveEvent);
            commandMap.mapEvent(UserActiveEvent.USER_ACTIVE, UserActiveCommand, UserActiveEvent);
        }
    }
}