/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 05/02/2014
 * Time: 10:38
 * The Media Workshop Ltd
 */
package tmw.exhibit.service
{
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import org.robotlegs.mvcs.Actor;

    import tmw.exhibit.controller.events.RestartEvent;

    import tmw.exhibit.controller.events.TimeoutEvent;

    public class TimerService extends Actor implements ITimerService
    {
        private var userActiveTimer:Timer;
        private var cancelTimeoutTimer:Timer;

        private var _userActiveWait:int;
        private var _cancelTimeoutWait:int;

        private var _isTiming:Boolean;

        public function TimerService()
        {
            userActiveTimer    = new Timer(1000);
            cancelTimeoutTimer = new Timer(1000);
        }

        public function setupTimers(userActiveWait:int, cancelTimeoutWait:int):void
        {
            _userActiveWait = userActiveWait;
            _cancelTimeoutWait = cancelTimeoutWait;

            userActiveTimer.addEventListener(TimerEvent.TIMER, userActiveTimerTick);
            cancelTimeoutTimer.addEventListener(TimerEvent.TIMER, cancelTimeoutTimerTick);
        }

        public function startUserActiveTimer():void
        {
            _isTiming = true;
            userActiveTimer.start();
        }

        public function startCancelTimeoutTimer():void
        {
            cancelTimeoutTimer.start();
        }

        public function resetUserActiveTimer():void
        {
            userActiveTimer.reset();
        }

        public function resetCancelTimeoutTimer():void
        {
            cancelTimeoutTimer.reset();
        }

        public function stopUserActiveTimer():void
        {
            _isTiming = false;
            userActiveTimer.stop();
        }

        public function stopCancelTimeoutTimer():void
        {
            cancelTimeoutTimer.stop();
        }

        private function userActiveTimerTick(event:TimerEvent):void
        {
            if(userActiveTimer.currentCount == _userActiveWait)
            {
                userActiveTimer.stop();
                userActiveTimer.reset();
                userActiveTimerComplete();

                cancelTimeoutTimer.reset();
                cancelTimeoutTimer.start();
            }
        }

        private function userActiveTimerComplete():void
        {
            userActiveTimer.stop();
            userActiveTimer.reset();
            dispatch(new TimeoutEvent(TimeoutEvent.SHOW_WARNING));
            dispatch(new TimeoutEvent(TimeoutEvent.COUNTDOWN, _cancelTimeoutWait - cancelTimeoutTimer.currentCount));
        }

        private function cancelTimeoutTimerTick(event:TimerEvent):void
        {
            dispatch(new TimeoutEvent(TimeoutEvent.COUNTDOWN, _cancelTimeoutWait - cancelTimeoutTimer.currentCount));
            if(cancelTimeoutTimer.currentCount == _cancelTimeoutWait)
            {

                cancelTimeoutTimer.stop();
                cancelTimeoutTimer.reset();
                cancelTimeoutTimerComplete();
            }
        }

        private function cancelTimeoutTimerComplete():void
        {
            dispatch(new RestartEvent(RestartEvent.RESTART));
        }

        public function get isTiming():Boolean
        {
            return _isTiming;
        }
    }
}
