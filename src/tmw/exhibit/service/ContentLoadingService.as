/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 22/10/2013
 * Time: 11:00
 * The Media Workshop Ltd
 */
package tmw.exhibit.service
{
    import com.greensock.events.LoaderEvent;
    import com.greensock.loading.ImageLoader;
    import com.greensock.loading.LoaderMax;
    import com.greensock.loading.MP3Loader;
    import com.greensock.loading.SWFLoader;
    import com.greensock.loading.VideoLoader;
    import com.greensock.loading.XMLLoader;
    import com.greensock.loading.data.XMLLoaderVars;

    import flash.text.Font;

    import org.robotlegs.mvcs.Actor;

    import tmw.exhibit.controller.events.ContentLoadEvent;

    public class ContentLoadingService extends Actor implements IContentLoadingService
    {

        private var _rawXML:XML;

        public function load(xmlPath:String):void
        {
            LoaderMax.activate([ImageLoader, VideoLoader, MP3Loader, SWFLoader]);

            var vars:XMLLoaderVars = new XMLLoaderVars();
            vars.onProgress(onProgress);
            vars.onComplete(onComplete);
            vars.onRawLoad(onRawLoad);
            vars.onError(onError);
            vars.onFail(onFail);

            var loader:XMLLoader = new XMLLoader(xmlPath, vars);
            loader.load();
        }

        private function onRawLoad(event:LoaderEvent):void
        {
            _rawXML = event.data;
        }

        private function onProgress(event:LoaderEvent):void
        {
            // dispatch progress events here
        }

        private function onComplete(event:LoaderEvent):void
        {
            var fonts:Array = Font.enumerateFonts();
            fonts.sortOn("fontName",Array.CASEINSENSITIVE);
            for (var i:int=0;i<fonts.length;i++)
            {
                trace("Embedded font = " + fonts[i].fontName + ", " + fonts[i].fontStyle);
            }
            dispatch(new ContentLoadEvent(ContentLoadEvent.COMPLETE, _rawXML));
        }

        private function onError(event:LoaderEvent):void
        {
            throw new Error(event.text);
        }

        private function onFail(event:LoaderEvent):void
        {
            throw new Error(event.text);
        }
    }
}
