/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 22/10/2013
 * Time: 15:43
 * The Media Workshop Ltd
 */
package tmw.exhibit.service
{
    public interface IContentLoadingService
    {
        function load(xmlPath:String):void;
    }
}
