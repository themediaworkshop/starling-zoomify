/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 05/02/2014
 * Time: 10:39
 * The Media Workshop Ltd
 */
package tmw.exhibit.service
{
    public interface ITimerService
    {
        function setupTimers(userActiveWait:int, cancelTimeoutWait:int):void;
        function startUserActiveTimer():void;
        function startCancelTimeoutTimer():void;
        function resetUserActiveTimer():void;
        function resetCancelTimeoutTimer():void;
        function stopUserActiveTimer():void;
        function stopCancelTimeoutTimer():void;
        function get isTiming():Boolean;
    }
}
