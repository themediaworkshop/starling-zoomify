/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 06/01/2014
 * Time: 16:37
 * The Media Workshop Ltd
 */
package tmw.exhibit.model
{
    import com.greensock.loading.LoaderMax;

    import flash.display.Bitmap;

    public class ContentLoader implements IContentLoader
    {
        public function getContent(nameOrURL:String):*
        {
            return LoaderMax.getContent(nameOrURL);
        }
        public function getClonedContent(nameOrURL:String):*
        {
            return new Bitmap(LoaderMax.getContent(nameOrURL).rawContent.bitmapData.clone());
        }

        public function getLoader(nameOrURL:String):*
        {
            return LoaderMax.getLoader(nameOrURL);
        }
    }
}
