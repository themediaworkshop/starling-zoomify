/**
 * Created by: Darren Perry <darren@themediaworkshop.co.uk>
 * Date: 12/09/13
 * Time: 14:33
 *
 */
package tmw.exhibit.model
{
    public interface IConfigModel
    {
        function loadData(data:Object):Boolean;
        function get data():Object;
    }
}
