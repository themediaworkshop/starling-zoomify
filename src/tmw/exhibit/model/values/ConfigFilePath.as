/**
 * Created by: Darren Perry <darren@themediaworkshop.co.uk>
 * Date: 12/09/13
 * Time: 14:07
 *
 */
package tmw.exhibit.model.values
{
    public class ConfigFilePath extends StringVO
    {
        public function ConfigFilePath(filePath:String)
        {
            super(filePath);
        }
    }
}
