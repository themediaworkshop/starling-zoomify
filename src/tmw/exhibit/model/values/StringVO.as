/**
 * Created by: Darren Perry <darren@themediaworkshop.co.uk>
 * Date: 12/09/13
 * Time: 14:07
 *
 */
package tmw.exhibit.model.values
{
    public class StringVO
    {
        protected var _value:String;

        public function StringVO(text:String)
        {
            _value = text;
        }

        public function toString():String
        {
            return _value;
        }
    }
}

