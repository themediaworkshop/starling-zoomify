/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 29/10/2013
 * Time: 16:34
 * The Media Workshop Ltd
 */
package tmw.exhibit.model.values
{
    import flash.display.NativeWindow;
    import flash.display.Sprite;

    import org.robotlegs.base.MediatorMap;
    import org.robotlegs.core.IInjector;
    import org.robotlegs.core.IReflector;

    public class WindowVO
    {
        private var _rootView:Sprite;
        private var _mediatorMap:MediatorMap;
        private var _x:Number;
        private var _y:Number;
        private var _width:Number;
        private var _height:Number;

        public function WindowVO(window:NativeWindow, rootView:Sprite)
        {
            _x = window.x;
            _y = window.y;
            _width = window.width;
            _height = window.height;
            _rootView = rootView;
        }

        public function setupMediatorMap(injector:IInjector, reflector:IReflector):void
        {
            _mediatorMap = new MediatorMap(_rootView, injector, reflector);
        }

        public function get mediatorMap():MediatorMap
        {
            return _mediatorMap;
        }

        public function get rootView():Sprite
        {
            return _rootView;
        }

        public function get x():Number
        {
            return _x;
        }

        public function get y():Number
        {
            return _y;
        }

        public function get width():Number
        {
            return _width;
        }

        public function get height():Number
        {
            return _height;
        }
    }
}
