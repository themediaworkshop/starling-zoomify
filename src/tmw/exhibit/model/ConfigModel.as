/**
 * Created by: Darren Perry <darren@themediaworkshop.co.uk>
 * Date: 12/09/13
 * Time: 14:32
 *
 */
package tmw.exhibit.model
{
    public class ConfigModel implements IConfigModel
    {
        private var _data:XML;

        public function loadData(data:Object):Boolean
        {
            _data = XML(data);
            if(_data) return true;
            else return false;
        }

        public function get data():Object
        {
            return _data;
        }
    }
}
