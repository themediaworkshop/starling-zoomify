/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 29/10/2013
 * Time: 12:59
 * The Media Workshop Ltd
 */
package tmw.exhibit.model
{
    import tmw.exhibit.model.values.WindowVO;

    public class WindowsModel implements IWindowsModel
    {

        private var _windows:Vector.<WindowVO>;

        public function WindowsModel()
        {
            _windows = new <WindowVO>[];
        }

        public function push(_value:WindowVO):void
        {
            _windows.push(_value);
        }

        public function get windows():Vector.<WindowVO>
        {
            return _windows;
        }
    }
}
