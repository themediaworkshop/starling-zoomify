/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 06/01/2014
 * Time: 16:36
 * The Media Workshop Ltd
 */
package tmw.exhibit.model
{
    public interface IContentLoader
    {
        function getContent(nameOrURL:String):*;
        function getClonedContent(nameOrURL:String):*;
        function getLoader(nameOrURL:String):*;
    }
}
