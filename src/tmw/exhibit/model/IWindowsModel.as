/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 29/10/2013
 * Time: 12:59
 * The Media Workshop Ltd
 */
package tmw.exhibit.model
{
    import tmw.exhibit.model.values.WindowVO;

    public interface IWindowsModel
    {
        function get windows():Vector.<WindowVO>;
        function push(_value:WindowVO):void;
    }
}
