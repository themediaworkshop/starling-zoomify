/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 07/03/2014
 * Time: 12:38
 * The Media Workshop Ltd
 */
package tmw.exhibit.model
{
    public interface IExhibitStateModel
    {
        function set loadComplete(value:Boolean):void;
        function get loadComplete():Boolean;
    }
}
