/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 07/03/2014
 * Time: 12:37
 * The Media Workshop Ltd
 */
package tmw.exhibit.model
{
    public class ExhibitStateModel implements IExhibitStateModel
    {
        private var _loadComplete:Boolean;

        public function set loadComplete(value:Boolean):void
        {
            _loadComplete = value;
        }

        public function get loadComplete():Boolean
        {
            return _loadComplete;
        }
    }
}
