/**
 * Created by: Darren Perry <darren@themediaworkshop.co.uk>
 * Date: 12/09/13
 * Time: 13:43
 *
 */
package tmw.exhibit
{
    import com.furusystems.dconsole2.DConsole;

    import flash.events.Event;
    import flash.events.KeyboardEvent;

    import flash.events.MouseEvent;

    import tmw.debug.FpsCounter;
    import tmw.debug.VersionNumber;

    import tmw.exhibit.controller.bootstraps.BootstrapCommands;
    import tmw.exhibit.controller.bootstraps.BootstrapConfigValues;
    import tmw.exhibit.controller.bootstraps.BootstrapModels;
    import tmw.exhibit.controller.bootstraps.BootstrapServices;
    import tmw.exhibit.controller.bootstraps.BootstrapViewMediators;

    import flash.display.DisplayObjectContainer;

    import org.robotlegs.mvcs.Context;

    import tmw.exhibit.controller.events.UserActiveEvent;

    import tmw.exhibit.controller.events.ViewSetupEvent;

    import tmw.exhibit.model.IWindowsModel;
    import tmw.exhibit.model.values.WindowVO;

    public class ExhibitContext extends Context
    {
        protected var _windowsModel:IWindowsModel;

        public function ExhibitContext(contextView:DisplayObjectContainer)
        {
            super(contextView, true);
        }

        override public function startup():void
        {
            new BootstrapConfigValues(injector);
            new BootstrapModels(injector);
            new BootstrapServices(injector);
            new BootstrapCommands(commandMap);

            addEventListener(ViewSetupEvent.SETUP_ROOT_VIEWS, evtSetupRootViews);

            super.startup();
        }

        protected function evtSetupRootViews(event:ViewSetupEvent):void
        {
            removeEventListener(ViewSetupEvent.SETUP_ROOT_VIEWS, evtSetupRootViews);

            _windowsModel = injector.getInstance(IWindowsModel);

            for each(var window:WindowVO in _windowsModel.windows)
            {
                window.setupMediatorMap(injector, reflector);
            }

            // setup framework view mediators
            new BootstrapViewMediators(_windowsModel.windows);

            // add views
            addDebug();
            addTimeoutTimers();
            addMainViews();
        }

        protected function addDebug():void
        {
            _windowsModel.windows[0].rootView.addChild(DConsole.view);

            var fps:FpsCounter = new FpsCounter(_windowsModel.windows[0].rootView);
            fps.x = 10;
            fps.y = _windowsModel.windows[0].height - fps.height - 10;
            fps.hideCounter();
            _windowsModel.windows[0].rootView.addChild(fps);

            var version:VersionNumber = new VersionNumber();
            version.y = _windowsModel.windows[0].height - version.height;
            version.x = _windowsModel.windows[0].width - version.width;
            _windowsModel.windows[0].rootView.addChild(version);
        }

        protected function addTimeoutTimers():void
        {
            for each(var window:WindowVO in _windowsModel.windows)
            {
                window.rootView.stage.addEventListener(MouseEvent.CLICK, evtUserActive);
                window.rootView.stage.addEventListener(MouseEvent.MOUSE_MOVE, evtUserActive);
                window.rootView.stage.addEventListener(MouseEvent.MOUSE_DOWN, evtUserActive);
                window.rootView.stage.addEventListener(MouseEvent.MOUSE_UP, evtUserActive);
                window.rootView.stage.addEventListener(KeyboardEvent.KEY_UP, evtUserActive);
                window.rootView.stage.addEventListener(KeyboardEvent.KEY_DOWN, evtUserActive);
            }
        }

        protected function evtUserActive(event:Event):void
        {
            _eventDispatcher.dispatchEvent(new UserActiveEvent(UserActiveEvent.USER_ACTIVE));
        }

        /* Override this function to populate windows */
        protected function addMainViews():void {}
    }
}
