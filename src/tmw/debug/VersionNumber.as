/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 30/10/2013
 * Time: 17:57
 * The Media Workshop Ltd
 */
package tmw.debug
{
    import flash.desktop.NativeApplication;
    import flash.display.DisplayObjectContainer;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;
    import flash.ui.Keyboard;

    public class VersionNumber extends Sprite
    {

        private var parentSprite:DisplayObjectContainer;

        public function VersionNumber()
        {
            if(!stage) addEventListener(Event.ADDED_TO_STAGE, evtAddedToStage);
            else evtAddedToStage();

            var tf:TextField = new TextField();
            var fmt:TextFormat = new TextFormat("Verdana", 14, 0xFFFFFF);
            fmt.bold = "bold";
            tf.defaultTextFormat = fmt;
            tf.autoSize = TextFieldAutoSize.LEFT;
            var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
            var ns:Namespace = appXml.namespace();
            tf.text = "Version: "+appXml.ns::versionNumber;
            addChild(tf);
            tf.x = tf.y = 3;
            graphics.beginFill(0x000000);
            graphics.drawRect(0,0,tf.width+6,tf.height+6);
        }

        private function evtAddedToStage(event:Event = null):void
        {
            if(hasEventListener(Event.ADDED_TO_STAGE)) removeEventListener(Event.ADDED_TO_STAGE, evtAddedToStage);
            parentSprite = parent;
            stage.addEventListener(KeyboardEvent.KEY_UP, evtKeyUp);
            hide();
        }

        private function evtKeyUp(event:KeyboardEvent):void
        {
            if(event.keyCode == Keyboard.F1) toggle();
        }

        public function show():void
        {
            if(!parentSprite.getChildByName(this.name)) parentSprite.addChild(this);
            parentSprite.setChildIndex(this, parentSprite.numChildren-1);
        }

        public function hide():void
        {
            if(parentSprite.getChildByName(this.name)) parentSprite.removeChild(this);
        }

        private function toggle():void
        {
            if(parentSprite.getChildByName(this.name)) hide();
            else show();
        }
    }
}
