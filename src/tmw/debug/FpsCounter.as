package tmw.debug
{

    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.Sprite;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.system.System;
    import flash.utils.getTimer;
    import flash.utils.setInterval;
    import flash.utils.clearInterval;

    /**
     * ...
     * @author Luke Angell
     *@created: 01/12/08
     *@updated: 08/12/08
     *@desc:
     * 	var myCounter:FpsCounter = new FpsCounter(this);
     * 	addChild(myCounter);
     *
     *	>> Default show/hide command - [ Ctrl Shift > ]
     *  >> Default reset command - [ Ctrl Shift Alt > ]
     *
     **/

        //TODO: re-evaluate max and mins over timeperios

    public class FpsCounter extends Sprite{
        private var lastTick:Number;

        private var mainCounter:Sprite;
        private var txt_fps:TextField;
        private var txt_mem:TextField;
        private var maxLabel:TextField;
        private var txt_lbl:TextField;
        private var txt_caption:TextField;

        private var graph_fps:Sprite;

        private var graphH:int;
        private var graphW:int;

        private var graphArray:Array;

        private var intID:uint;

        private var tFPS:int;
        private var tMem:int;

        private var mainInterval:int;
        private var MAXFPS:int;
        private var MAXMEM:int;
        private var MINFPS:int;

        private var initVars:Object;

        private var drawGraph:Boolean;
        private var mainSprite:DisplayObject;

        private var key:String;
        private var SHOWCOUNTER:Boolean = true;


        //MAIN()-------------------------------------------------------------------------------------------------^^^^^^
        //-------------------------------------------------------------------------------------------------------^^^^^^
        public function FpsCounter(tSprite:DisplayObject,  tInterval:int = 1000, tMaxFps:int = 30, tMaxMem:int = 10000, tDrawGraph:Boolean = true, tKey:String=">"):void{
            mainSprite = tSprite
            mainInterval = tInterval;
            drawGraph = tDrawGraph
            key = tKey;

            initVars = {fps:tMaxFps, mem:tMaxMem};

            resetCounter();

            graphH = 50;
            graphW = 200;

            mainCounter = new Sprite();

            //create fps lbl---------------------
            txt_fps = createTextBox(0xFF0000);
            txt_fps.text = "FPS: " + MAXFPS;
            mainCounter.addChild(txt_fps);


            //create memory lbl------------------
            txt_mem = createTextBox(0x00FF00);
            txt_mem.text = "Mem: " + MAXMEM + "Kb";
            txt_mem.x = 48;
            mainCounter.addChild(txt_mem);

            //create interval lbl----------------
            //txt_lbl = createTextBox(0x444444);
            //txt_lbl.text = "int: " + mainInterval/1000 + "s";
            //txt_lbl.x = graphW - txt_lbl.width;
            //mainCounter.addChild(txt_lbl);


            //create graph-----------------------
            graph_fps = createGraph();
            graph_fps.x = 1;
            graph_fps.y = 20;
            mainCounter.addChild(graph_fps);


            //create interval text---------------
            maxLabel = createTextBox(0x3399FF);
            getMaxLabels();
            maxLabel.x = graphW - maxLabel.width;
            maxLabel.y = mainCounter.height;// - maxLabel.height / 2 - 5
            mainCounter.addChild(maxLabel);

            //create caption---------------------
            txt_caption = createTextBox(0xCCCCCC);
            txt_caption.text = "int: " + mainInterval/1000 + "s";
            txt_caption.x = graphW - txt_caption.width;
            mainCounter.addChild(txt_caption);

            //draw bg----------------------------
            drawBG(graphW+2, mainCounter.height);

            graphArray = new Array();

            showCounter();

            mainSprite.stage.addEventListener(KeyboardEvent.KEY_DOWN, onReportKeyDown);
        }


        //ENTFRM()-----------------------------------------------------------------------------------------------######
        //-------------------------------------------------------------------------------------------------------######
        private function entFrm(evt:Event):void{
            //do fps--------------
            var t = getTimer();
            tFPS = int(1000 / (t - lastTick));

            //set fps cap---------
            if (tFPS > 60){ tFPS = 60; }

            lastTick = t;

            //do mem--------------
            tMem = int(System.totalMemory / 1024.0);

            if (tFPS > MAXFPS){ MAXFPS = tFPS; }
            if (tMem > MAXMEM){ MAXMEM = tMem; }
            if (tFPS < MINFPS){ MINFPS = tFPS; }

            getMaxLabels();
        }


        //DOGRAPH()----------------------------------------------------------------------------------------------######
        //-------------------------------------------------------------------------------------------------------######
        private function doGraph():void{
            (mainSprite as DisplayObjectContainer).setChildIndex(this, (mainSprite as DisplayObjectContainer).numChildren-1);

            //update text fields
            txt_mem.text = "Mem: " + numberToString(tMem) + "Kb";
            txt_fps.text = "FPS: " + String(tFPS);



            if(drawGraph == true){
                //add to graph--------------------
                if(graphArray.length < graphW){
                    graphArray.push({mem: tMem, fps: tFPS});
                }else{
                    graphArray.splice(0, 1);
                    graphArray.push({mem: tMem, fps: tFPS});
                }
                //---------------------------------

                graph_fps.graphics.clear();

                graph_fps.graphics.lineStyle(1, 0x3399FF);
                graph_fps.graphics.moveTo(0, 0);
                graph_fps.graphics.lineTo(graphW, 0);
                graph_fps.graphics.lineStyle(1, 0xCCCCCC);
                graph_fps.graphics.moveTo(0, graphH);
                graph_fps.graphics.lineTo(graphW, graphH);

                //draw 30 fps line------------------
                graph_fps.graphics.lineStyle(1, 0xCCCC33, 0.6);
                var tFPSLine:int = (graphH/100) * ((30/MAXFPS)*100);
                graph_fps.graphics.moveTo(0, graphH-tFPSLine);
                graph_fps.graphics.lineTo(graphW, graphH-tFPSLine);

                //draw 20 fps line
                graph_fps.graphics.lineStyle(1, 0xCCCC33, 0.2);
                tFPSLine = (graphH/100) * ((20/MAXFPS)*100);
                graph_fps.graphics.moveTo(0, graphH-tFPSLine);
                graph_fps.graphics.lineTo(graphW, graphH - tFPSLine);

                //draw 10 fps line
                graph_fps.graphics.lineStyle(1, 0xCCCC33, 0.2);
                tFPSLine = (graphH/100) * ((10/MAXFPS)*100);
                graph_fps.graphics.moveTo(0, graphH-tFPSLine);
                graph_fps.graphics.lineTo(graphW, graphH - tFPSLine);


                //draw fps-------------------------
                graph_fps.graphics.lineStyle(1, 0xFF0000);
                for (var i:int = 0; i < graphArray.length; i++){
                    var tPercentFPS:int = (graphH/100) * ((graphArray[i].fps/MAXFPS)*100);

                    if (i == 0){
                        graph_fps.graphics.moveTo(0, graphH-tPercentFPS);
                    }

                    if (graphH - tPercentMem < 0){
                        graph_fps.graphics.lineTo(j, 0);
                    }else{
                        graph_fps.graphics.lineTo(i, graphH - tPercentFPS);
                    }
                }
                //---------------------------------


                //draw memory----------------------
                graph_fps.graphics.lineStyle(1, 0x00FF00);
                for (var j:int = 0; j < graphArray.length; j++){
                    var tPercentMem:int = (graphH/100) * ((graphArray[j].mem/MAXMEM)*100);

                    if (j == 0){
                        graph_fps.graphics.moveTo(0, graphH-tPercentMem);
                    }

                    if (graphH - tPercentMem < 0){
                        graph_fps.graphics.lineTo(j, 0);
                    }else{
                        graph_fps.graphics.lineTo(j, graphH-tPercentMem);
                    }
                }
                //---------------------------------

                graph_fps.graphics.endFill();
            }

        }



        //NUMBERTOSTRING()---------------------------------------------------------------------------------------######
        //-------------------------------------------------------------------------------------------------------######
        private function numberToString(n:Number):String {
            var s1:String = n.toString();
            var a1:Array = s1.split(".");
            var s2:String = "";
            var l = a1[0].length;

            while(l--) {
                if(l < a1[0].length - 1 && (a1[0].length - l - 1) % 3 == 0) s2 = "," + s2;
                s2 = a1[0].substr(l, 1) + s2;
            }

            if(a1.length > 1) s2 += "." + a1[1];

            return(s2);
        }


        //CREATETEXTBOX()----------------------------------------------------------------------------------------######
        //-------------------------------------------------------------------------------------------------------######
        private function createTextBox(tCol:uint):TextField{
            //text fps----------------------
            var tText:TextField = new TextField();
            tText.autoSize = TextFieldAutoSize.LEFT;
            tText.selectable = false;
            tText.wordWrap = false;
            //------------------------------

            var tFmt:TextFormat = new TextFormat();
            tFmt.size = 9;
            tFmt.color = tCol;
            tFmt.align = "right";
            tFmt.font = "Verdana";

            tText.defaultTextFormat = tFmt;

            return tText;
        }


        //GETMAXLABELS()-----------------------------------------------------------------------------------------######
        //-------------------------------------------------------------------------------------------------------######
        private function getMaxLabels():void{
            if(maxLabel != null){
                maxLabel.text = "Min: " + MINFPS + "FPS , Max: " + numberToString(MAXMEM) +"Kb/"+MAXFPS+"FPS";
                maxLabel.x = graphW - maxLabel.width;
            }
        }

        //CREATEGRAPH()------------------------------------------------------------------------------------------######
        //-------------------------------------------------------------------------------------------------------######
        private function createGraph():Sprite{
            var tSprite:Sprite = new Sprite();
            tSprite.graphics.drawRect(0, 0, graphW, 50)
            tSprite.graphics.endFill();
            return tSprite;
        }

        //DRAWBG()-----------------------------------------------------------------------------------------------######
        //-------------------------------------------------------------------------------------------------------######
        private function drawBG(tW:int, tH:int):void{
            mainCounter.graphics.beginFill(0x000000, 0.8);
            mainCounter.graphics.lineStyle(1, 0xCCCCCC);
            mainCounter.graphics.drawRect(0, 0, tW, tH);
            mainCounter.graphics.endFill();
        }

        //ONREPORTKEYDOWN()--------------------------------------------------------------------------------------^^^^^^
        //-------------------------------------------------------------------------------------------------------^^^^^^
        public function onReportKeyDown(evt:KeyboardEvent):void{
            switch(String.fromCharCode(evt.charCode).toLowerCase()){
                case key.toLowerCase():
                    if (evt.ctrlKey == true && evt.shiftKey == true && evt.altKey == true){
                        resetCounter();
                    }else if (evt.ctrlKey == true && evt.shiftKey == true){
                        if (SHOWCOUNTER == false){
                            showCounter();
                        }else{
                            hideCounter();
                        }
                    }

                    break;
            }

        }


        //RESETCOUNTER()-----------------------------------------------------------------------------------------^^^^^^
        //-------------------------------------------------------------------------------------------------------^^^^^^
        public function resetCounter():void{
            graphArray = new Array();
            MAXFPS = initVars.fps;
            MINFPS = initVars.fps;
            MAXMEM = initVars.mem;
        }



        //SHOWCOUNTER()------------------------------------------------------------------------------------------^^^^^^
        //-------------------------------------------------------------------------------------------------------^^^^^^
        public function showCounter():void{
            SHOWCOUNTER = true;
            this.addEventListener(Event.ENTER_FRAME, entFrm);
            intID = setInterval(doGraph, mainInterval);

            if(this.getChildByName(mainCounter.name) == null){
                addChild(mainCounter);
            }
        }

        //HIDECOUNTER()------------------------------------------------------------------------------------------^^^^^^
        //-------------------------------------------------------------------------------------------------------^^^^^^
        public function hideCounter():void {
            SHOWCOUNTER = false;
            this.removeEventListener(Event.ENTER_FRAME, entFrm);
            clearInterval(intID);

            if(this.getChildByName(mainCounter.name)){
                removeChild(mainCounter);
            }
        }
    }

}