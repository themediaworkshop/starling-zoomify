/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 10:43
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.view
{
    import starling.display.Image;
    import starling.display.Sprite;

    import tmw.arabianmaps.model.ITileModel;

    public class ZoomLevel extends Sprite
    {
        private var _tileModel:ITileModel;
        private var _zoomLevel:int;

        private var numWide:int,numHigh:int;
        private var _nativeWidth:Number,_nativeHeight:Number;

        private var tiles:Vector.<Vector.<Image>>;


        public function ZoomLevel(zoomLevel:int)
        {
            _zoomLevel = zoomLevel;
            super();
        }

        public function set tileModel(value:ITileModel):void
        {
            _tileModel = value;
        }

        public function init():void
        {

            tiles = new Vector.<Vector.<Image>>();

            var long:int = 0;
            var lat:int = 0;

            while(1)
            {
                tiles.push(new Vector.<Image>());
                if(_tileModel.getTileTexture(_zoomLevel,long,lat))
                {
                    while(1)
                    {
                        if(_tileModel.getTileTexture(_zoomLevel,long,lat))
                        {
                            var i:Image = _tileModel.getTileTexture(_zoomLevel,long,lat);
                            i.x = long * _tileModel.tileWidth;
                            i.y = lat * _tileModel.tileHeight;
                            addChild(i);
                            tiles[long].push(i);
                        }
                        else
                        {
                            numHigh = lat;
                            lat = 0;
                            break;
                        }
                        lat++;
                    }
                }
                else
                {
                    numWide = long;
                    long = 0;
                    break;
                }
                long++;
            }

            flatten();

            _nativeWidth = width;
            _nativeHeight = height;

        }

        public function get nativeWidth():Number
        {
            return _nativeWidth;
        }

        public function get nativeHeight():Number
        {
            return _nativeHeight;
        }
    }
}
