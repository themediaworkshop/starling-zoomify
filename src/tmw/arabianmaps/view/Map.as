/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 13:31
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.view
{
    import com.furusystems.dconsole2.DConsole;
    import com.greensock.TweenLite;

    import flash.geom.Point;
    import flash.ui.Keyboard;

    import starling.core.Starling;

    import starling.display.Sprite;
    import starling.events.KeyboardEvent;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;

    import tmw.arabianmaps.controller.events.StarlingMapEvent;

    import tmw.arabianmaps.model.ITileModel;

    public class Map extends Sprite
    {
        private var zoomLevels:Vector.<ZoomLevel>;
        private var maxZoom:ZoomLevel;
        private var currentZoom:ZoomLevel;

        public function Map()
        {

        }

        public function init(tileModel:ITileModel):void
        {
            zoomLevels = new Vector.<ZoomLevel>();
            for(var i:int = 0 ; i <= tileModel.maxZoom; i++)
            {
                var z:ZoomLevel = new ZoomLevel(i);
                z.tileModel = tileModel;
                z.init();
                zoomLevels.push(z);
            }
            maxZoom = currentZoom = zoomLevels[zoomLevels.length-1];
            maxZoom.x = (stage.stageWidth - maxZoom.width) /2;
            addChild(maxZoom);

            currentZoom.width = maxZoom.nativeWidth * (stage.stageWidth/maxZoom.nativeWidth);
            currentZoom.height = maxZoom.nativeHeight * (stage.stageWidth/maxZoom.nativeWidth);

            checkPanLimits();

            stage.addEventListener(TouchEvent.TOUCH, evtTouch);

            Starling.current.dispatchEvent(new StarlingMapEvent(StarlingMapEvent.ANCHOR_UPDATE, true));
        }

        private function evtTouch(event:TouchEvent):void
        {
            var touch:Touch = event.getTouch(this, TouchPhase.MOVED);
            if (touch)
            {
                var localPos:Point = touch.getMovement(this);
                currentZoom.x += localPos.x;
                currentZoom.y += localPos.y;
                checkPanLimits();
                Starling.current.dispatchEvent(new StarlingMapEvent(StarlingMapEvent.ANCHOR_UPDATE, true));
            }
        }

        public function mapToStagePoint(point:Point):Point
        {
            var rX:Number = (point.x*(currentZoom.width/maxZoom.nativeWidth))+ currentZoom.x;
            var rY:Number = (point.y*(currentZoom.height/maxZoom.nativeHeight))+currentZoom.y;
            return new Point(rX,rY);
        }

        private function evtKey(event:KeyboardEvent):void
        {
            switch(event.keyCode)
            {
            }
        }

        public function tweenTo(zoom:Number, x:Number, y:Number):void
        {
            if(zoom < 1) zoom = 1;
            var toWidth:Number = maxZoom.nativeWidth * (stage.stageWidth/maxZoom.nativeWidth) * zoom;
            var toHeight:Number = maxZoom.nativeHeight * (stage.stageWidth/maxZoom.nativeWidth) * zoom;

            var toX:Number = x * (maxZoom.nativeWidth/toWidth);
            var toY:Number = y * (maxZoom.nativeHeight/toHeight);

            TweenLite.to(currentZoom, 2, {
                width:toWidth,
                height:toHeight,
                x:toX,
                y:toY,
                onComplete:checkZoomLevel,
                onUpdate:onTweenUpdate
            });
        }

        private function onTweenUpdate():void
        {
            checkPanLimits();
            Starling.current.dispatchEvent(new StarlingMapEvent(StarlingMapEvent.ANCHOR_UPDATE, true));
        }

        public function checkZoomLevel():void
        {
            var i:int = 0;
            while(i++ < zoomLevels.length-1)
            {
                if(zoomLevels[i].stage && zoomLevels[i].width > zoomLevels[i].nativeWidth*1.5 && i < zoomLevels.length - 1)
                {
                    trace(i,"to",i+1);
                    zoomLevels[i+1].x = zoomLevels[i].x;
                    zoomLevels[i+1].y = zoomLevels[i].y;
                    zoomLevels[i+1].width = zoomLevels[i].width;
                    zoomLevels[i+1].height = zoomLevels[i].height;
                    addChild(zoomLevels[i+1]);
                    removeChild(zoomLevels[i]);
                    zoomLevels[i].width = zoomLevels[i].nativeWidth;
                    zoomLevels[i].height = zoomLevels[i].nativeHeight;
                }
                else if(zoomLevels[i].stage && zoomLevels[i].width < zoomLevels[i].nativeWidth*.75 && i > 0)
                {
                    trace(i,"to",i-1);
                    zoomLevels[i-1].x = zoomLevels[i].x;
                    zoomLevels[i-1].y = zoomLevels[i].y;
                    zoomLevels[i-1].width = zoomLevels[i].width;
                    zoomLevels[i-1].height = zoomLevels[i].height;
                    addChild(zoomLevels[i-1]);
                    removeChild(zoomLevels[i]);
                    zoomLevels[i].width = zoomLevels[i].nativeWidth;
                    zoomLevels[i].height = zoomLevels[i].nativeHeight;
                }
            }
            currentZoom = ZoomLevel(getChildAt(0));
        }

        private function checkPanLimits():void
        {
            if(currentZoom.x > 0) currentZoom.x = 0;
            else if(currentZoom.x < stage.stageWidth - currentZoom.width) currentZoom.x = stage.stageWidth - currentZoom.width;

            if(currentZoom.y > 0) currentZoom.y = 0;
            else if(currentZoom.y < stage.stageHeight - currentZoom.height) currentZoom.y = stage.stageHeight - currentZoom.height;
        }
    }
}
