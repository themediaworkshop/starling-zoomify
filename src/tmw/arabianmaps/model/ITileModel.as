/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 11:25
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.model
{
    import starling.display.Image;

    import tmw.arabianmaps.model.VO.TileVO;

    public interface ITileModel
    {
        function addTile(tile:TileVO):void;
        function getTileTexture(zoom:int,long:int,lat:int):Image;
        function get tileWidth():int;
        function get tileHeight():int;
        function get maxZoom():int;
    }
}
