/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 11:26
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.model
{

    import starling.display.Image;

    import tmw.arabianmaps.model.VO.TileVO;

    public class TileModel implements ITileModel
    {
        private var tiles:Vector.<TileVO>;
        private var _maxWidth:int;
        private var _maxHeight:int;
        private var _maxZoom:int;

        public function TileModel():void
        {
            tiles = new Vector.<TileVO>();
        }
        public function addTile(tile:TileVO):void
        {
            tiles.push(tile);
            _maxWidth = Math.max(tile.image.width, _maxWidth);
            _maxHeight = Math.max(tile.image.height, _maxHeight);
            _maxZoom = Math.max(tile.zoom, _maxZoom);
        }

        public function getTileTexture(zoom:int,long:int,lat:int):Image
        {
            for each(var tile:TileVO in tiles) if(tile.zoom == zoom && tile.long == long && tile.lat == lat) return tile.image;
            return null;
        }

        public function get tileWidth():int
        {
            return _maxWidth;
        }

        public function get tileHeight():int
        {
            return _maxHeight;
        }

        public function get maxZoom():int
        {
            return _maxZoom;
        }
    }
}
