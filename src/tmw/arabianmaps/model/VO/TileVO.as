/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 11:26
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.model.VO
{
    import starling.display.Image;

    public class TileVO
    {
        private var  _image:Image, _zoom:int, _long:int, _lat:int;

        public function TileVO(image:Image, zoom:int, long:int, lat:int):void
        {
            _image = image;
            _zoom = zoom;
            _long = long;
            _lat = lat;
        }

        public function get image():Image
        {
            return _image;
        }

        public function get zoom():int
        {
            return _zoom;
        }

        public function get long():int
        {
            return _long;
        }

        public function get lat():int
        {
            return _lat;
        }
    }
}
