/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 15:50
 * The Media Workshop Ltd
 */
package tmw.arabianmaps
{
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.geom.Point;

    import flashx.textLayout.formats.TextAlign;

    import starling.core.Starling;
    import starling.events.Event;

    import tmw.arabianmaps.controller.events.MapEvent;

    import tmw.arabianmaps.controller.events.StarlingMapEvent;

    import tmw.arabianmaps.model.ITileModel;
    import tmw.arabianmaps.view.Map;
    import tmw.text.DynamicTextField;
    import tmw.text.SimpleTextField;
    import tmw.text.Text;
    import tmw.text.TextFactory;

    public class ArabianMaps extends Sprite
    {
        public var configDone:Boolean;
        private var _progressBar:Sprite;
        private var _progressText:DynamicTextField;

        private var _starling:Starling;

        private var _tileModel:ITileModel;

        public function ArabianMaps()
        {

        }

        public function addContent():void
        {
            configDone = true;
            _progressBar = new Sprite();
            addChild(_progressBar);
            _progressText = new DynamicTextField(TextFactory.createTextXML("", Text.LANGUAGE_ENGLISH, 300, NaN, "TheSansArabic", 20, "#000000", TextAlign.CENTER));
            _progressBar.addChild(_progressText);
            _progressText.y = 130;

            var t:SimpleTextField = new SimpleTextField(TextFactory.createTextXML("Loading Map Tiles...", Text.LANGUAGE_ENGLISH, 500, NaN, "TheSansArabic", 20, "#000000", TextAlign.CENTER));
            _progressBar.addChild(t);
            t.x = -100;
            t.y = -t.height - 30;

            if(!stage) addEventListener(flash.events.Event.ADDED_TO_STAGE, evtStage);
            else evtStage();
        }

        private function evtStage(event:flash.events.Event = null):void
        {
            _progressBar.x = (stage.stageWidth / 2) - 150;
            _progressBar.y = (stage.stageHeight / 2) -50;
            _starling = new Starling(Map, stage);
            _starling.addEventListener(StarlingMapEvent.ANCHOR_UPDATE, evtPan);
            _starling.start();
        }

        public function progress(progress:Number):void
        {
            with(_progressBar.graphics)
            {
                clear();
                lineStyle(5, 0x000000);
                drawRect(0,0,300,100);
                lineStyle(0);
                beginFill(0x000000);
                drawRect(10,10,280*progress,80);
            }
            _progressText.text = Math.floor(100*progress).toString()+"%";
        }

        public function init(tileModel:ITileModel):void
        {
            removeChild(_progressBar);
            _progressBar = null;

            _tileModel = tileModel;
            if(_starling.context) Map(_starling.root).init(_tileModel);
            else _starling.addEventListener(starling.events.Event.CONTEXT3D_CREATE, evtRootCreated);
        }

        private function evtRootCreated(event:starling.events.Event):void
        {
            _starling.removeEventListener(starling.events.Event.CONTEXT3D_CREATE, evtRootCreated);
            Map(_starling.root).init(_tileModel);
        }

        private function evtPan(event:StarlingMapEvent):void
        {
            dispatchEvent(new MapEvent(MapEvent.ANCHOR_UPDATE));
        }

        public function mapToStagePoint(point:Point):Point
        {
            return Map(_starling.root).mapToStagePoint(point);
        }

        public function tweenTo(zoom:Number, x:Number, y:Number):void
        {
            Map(_starling.root).tweenTo(zoom,x,y);
        }
    }
}
