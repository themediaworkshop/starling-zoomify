/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 10:48
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.service
{
    import com.greensock.events.LoaderEvent;
    import com.greensock.loading.ImageLoader;
    import com.greensock.loading.LoaderMax;

    import flash.filesystem.File;

    import org.robotlegs.mvcs.Actor;

    import starling.display.Image;

    import tmw.arabianmaps.controller.events.TileLoaderEvent;

    import tmw.arabianmaps.model.ITileModel;
    import tmw.arabianmaps.model.VO.TileVO;

    public class TileLoader extends Actor implements ITileLoader
    {
        [Inject]
        public var tileModel:ITileModel;

        private var queue:LoaderMax;

        public function TileLoader()
        {
            queue = new LoaderMax();
        }

        public function loadTiles(folder:String):void
        {
            trace("Load Tiles");
            var zoom:int = 0;
            var long:int = 0;
            var lat:int = 0;

            var file:File;
            while(1)
            {
                file = File.applicationDirectory.resolvePath(folder + "/"+zoom+"-"+long+"-"+lat+".jpg");
                if(file.exists)
                {
                    while(1)
                    {
                        file = File.applicationDirectory.resolvePath(folder + "/"+zoom+"-"+long+"-"+lat+".jpg");
                        if(file.exists)
                        {
                            while(1)
                            {
                                file = File.applicationDirectory.resolvePath(folder + "/"+zoom+"-"+long+"-"+lat+".jpg");
                                if(file.exists)
                                    queue.append(new ImageLoader(file.url, {zoom:zoom,long:long,lat:lat}));
                                else
                                {
                                    lat = 0;
                                    break;
                                }
                                lat++;
                            }
                            long++;
                        }
                        else
                        {
                            long = 0;
                            break;
                        }
                    }
                    zoom++;
                }
                else break;
            }

            queue.addEventListener(LoaderEvent.CHILD_COMPLETE, evtChildComplete);
            queue.addEventListener(LoaderEvent.PROGRESS, evtProgress);
            queue.addEventListener(LoaderEvent.COMPLETE, evtComplete);
            queue.load();
        }

        private function evtChildComplete(event:LoaderEvent):void
        {

            tileModel.addTile(new TileVO(Image.fromBitmap(ImageLoader(event.target).rawContent), event.target.vars.zoom,event.target.vars.long,event.target.vars.lat));
            //trace(event.target.vars.zoom);
        }

        private function evtComplete(event:LoaderEvent):void
        {
            _eventDispatcher.dispatchEvent(new TileLoaderEvent(TileLoaderEvent.COMPLETE));
            trace("Complete:",queue.status, queue.numChildren, queue.rawProgress);
        }

        private function evtProgress(event:LoaderEvent):void
        {
            dispatch(new TileLoaderEvent(TileLoaderEvent.PROGRESS,queue.rawProgress));
            //trace(queue.status, queue.numChildren, queue.rawProgress);
        }
    }
}
