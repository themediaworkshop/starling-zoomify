/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 10:46
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.service
{
    public interface ITileLoader
    {
        function loadTiles(folder:String):void;
    }
}
