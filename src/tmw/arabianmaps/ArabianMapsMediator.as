/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 26/03/2014
 * Time: 09:27
 * The Media Workshop Ltd
 */
package tmw.arabianmaps
{
    import org.robotlegs.mvcs.Mediator;

    import tmw.arabianmaps.controller.events.TileLoaderEvent;

    import tmw.arabianmaps.model.ITileModel;
    import tmw.exhibit.controller.events.ViewRegisteredEvent;
    import tmw.exhibit.controller.events.ViewSetupEvent;

    public class ArabianMapsMediator extends Mediator
    {
        [Inject]
        public var view:ArabianMaps;

        [Inject]
        public var tileModel:ITileModel;

        override public function onRegister():void
        {
            addContextListener(ViewSetupEvent.APPLY_CONFIG, evtApplyConfig);
            dispatch(new ViewRegisteredEvent(ViewRegisteredEvent.VIEW_REGISTERED));
        }

        private function evtApplyConfig(event:ViewSetupEvent):void
        {
            if(view.configDone) return;
            removeContextListener(ViewSetupEvent.APPLY_CONFIG, evtApplyConfig);
            view.addContent();

            dispatch(new TileLoaderEvent(TileLoaderEvent.LOAD));
            addContextListener(TileLoaderEvent.PROGRESS, evtTilesProgress);
            addContextListener(TileLoaderEvent.COMPLETE, evtTilesLoaded);
        }

        private function evtTilesProgress(event:TileLoaderEvent):void
        {
            view.progress(event.progress);
        }

        private function evtTilesLoaded(event:TileLoaderEvent):void
        {
            removeContextListener(TileLoaderEvent.PROGRESS, evtTilesProgress);
            removeContextListener(TileLoaderEvent.COMPLETE, evtTilesLoaded);
            view.init(tileModel);
        }
    }
}
