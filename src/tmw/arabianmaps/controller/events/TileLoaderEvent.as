/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 11:37
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.controller.events
{
    import flash.events.Event;

    public class TileLoaderEvent extends Event
    {

        public static const LOAD:String = "TileLoaderEvent.load";
        public static const PROGRESS:String = "TileLoaderEvent.progress";
        public static const COMPLETE:String = "TileLoaderEvent.complete";

        private var _progress:Number;

        public function TileLoaderEvent(type:String, progress:Number = NaN, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            _progress = progress;
            super(type, bubbles, cancelable);
        }

        override public function clone():Event
        {
            return new TileLoaderEvent(type,progress,bubbles,cancelable);
        }

        public function get progress():Number
        {
            return _progress;
        }

    }
}
