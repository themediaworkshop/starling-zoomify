/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 16:19
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.controller.events
{
    import starling.events.Event;

    public class StarlingMapEvent extends Event
    {

        public static const ANCHOR_UPDATE:String = "StarlingMapEvent.anchorUpdate";

        public function StarlingMapEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }

    }
}
