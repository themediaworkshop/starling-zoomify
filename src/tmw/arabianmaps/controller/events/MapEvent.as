/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 17:24
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.controller.events
{
    import flash.events.Event;

    public class MapEvent extends Event
    {

        public static const READY:String = "MapEvent.ready";
        public static const ANCHOR_UPDATE:String = "MapEvent.anchorUpdate";

        public function MapEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }
    }
}
