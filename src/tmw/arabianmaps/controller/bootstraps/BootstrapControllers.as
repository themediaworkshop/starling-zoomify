/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 26/03/2014
 * Time: 09:19
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.controller.bootstraps
{
    import org.robotlegs.core.ICommandMap;
    import org.robotlegs.core.IInjector;

    import tmw.arabianmaps.controller.commands.StartupCommand;
    import tmw.arabianmaps.controller.events.TileLoaderEvent;

    import tmw.arabianmaps.model.ITileModel;
    import tmw.arabianmaps.model.TileModel;

    import tmw.arabianmaps.service.ITileLoader;
    import tmw.arabianmaps.service.TileLoader;

    public class BootstrapControllers
    {
        public function BootstrapControllers(_injector:IInjector, _commandMap:ICommandMap)
        {
            _injector.mapSingletonOf(ITileLoader, TileLoader);
            _injector.mapSingletonOf(ITileModel, TileModel);

            _commandMap.mapEvent(TileLoaderEvent.LOAD, StartupCommand, TileLoaderEvent, true);
        }
    }
}
