/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 26/03/2014
 * Time: 09:53
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.controller.bootstraps
{
    import org.robotlegs.core.IMediatorMap;

    import tmw.arabianmaps.ArabianMaps;

    import tmw.arabianmaps.ArabianMapsMediator;

    public class BootstrapMediators
    {
        public function BootstrapMediators(_mediatorMap:IMediatorMap)
        {
            _mediatorMap.mapView(ArabianMaps, ArabianMapsMediator);
        }
    }
}
