/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 11:11
 * The Media Workshop Ltd
 */
package tmw.arabianmaps.controller.commands
{
    import org.robotlegs.mvcs.Command;

    import tmw.arabianmaps.service.ITileLoader;

    public class StartupCommand extends Command
    {
        [Inject]
        public var tileLoader:ITileLoader;

        override public function execute():void
        {
            tileLoader.loadTiles("assets/Map1");
        }
    }
}
