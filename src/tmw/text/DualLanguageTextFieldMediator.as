/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 24/10/2013
 * Time: 15:38
 * The Media Workshop Ltd
 */
package tmw.text
{
    import org.robotlegs.mvcs.Mediator;

    import tmw.exhibit.controller.events.ChangeLanguageEvent;

    public class DualLanguageTextFieldMediator extends Mediator
    {

        [Inject]
        public var view:DualLanguageTextField;

        override public function onRegister():void
        {
            addContextListener(ChangeLanguageEvent.CHANGE_TO_ENGLISH, evtShowEnglish);
            addContextListener(ChangeLanguageEvent.CHANGE_TO_ARABIC, evtShowArabic);
            view.showEnglish();
        }

        private function evtShowEnglish(event:ChangeLanguageEvent):void
        {
            view.showEnglish();
        }

        private function evtShowArabic(event:ChangeLanguageEvent):void
        {
            view.showArabic();
        }
    }
}
