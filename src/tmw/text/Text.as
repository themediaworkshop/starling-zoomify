/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 30/10/2013
 * Time: 12:49
 * The Media Workshop Ltd
 */
package tmw.text
{
    public class Text
    {
        public static const LANGUAGE_ENGLISH:String = "english";
        public static const LANGUAGE_ARABIC:String = "arabic";
    }
}
