/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 30/10/2013
 * Time: 12:33
 * The Media Workshop Ltd
 */
package tmw.text
{
    public class DynamicTextField extends SimpleTextField
    {
        public function DynamicTextField(xml:XML)
        {
            super(xml);
        }

        public function set textXML(value:XML):void
        {
            _textXML = value;
            buildTextFlow();
        }

        public function set text(value:String):void
        {
            _textXML.p = value;
            buildTextFlow();
        }

        public function get text():String
        {
            return _textXML.p;
        }
    }
}
