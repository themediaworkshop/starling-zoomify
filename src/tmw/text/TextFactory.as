/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 30/10/2013
 * Time: 12:40
 * The Media Workshop Ltd
 */
package tmw.text
{
    public class TextFactory
    {
        /*
         * Use this function to create simple text.
         * For more advanced styling just create your XML/TextFlow manually.
         */
        public static function createTextXML(text:String, language:String, width:Number = NaN, height:Number = NaN, fontFamily:String = null, fontSize:Number = NaN, color:String = null, textAlign:String = null):XML
        {
            var returnXML:XML = XML("<"+language+"><p>"+text+"</p></"+language+">");

            if(width)       returnXML.@width        = width;
            if(height)      returnXML.@height       = height;
            if(fontFamily)  returnXML.p.@fontFamily = fontFamily;
            if(fontSize)    returnXML.p.@fontSize   = fontSize;
            if(color)       returnXML.p.@color      = color;
            if(textAlign)   returnXML.p.@textAlign  = textAlign;

            return returnXML;
        }
    }
}
