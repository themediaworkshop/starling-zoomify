/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 24/10/2013
 * Time: 10:11
 * The Media Workshop Ltd
 */
package tmw.text
{
    import flash.display.Sprite;
    import flash.text.engine.FontLookup;
    import flash.text.engine.RenderingMode;

    import flashx.textLayout.container.ContainerController;

    import flashx.textLayout.conversion.TextConverter;

    import flashx.textLayout.elements.TextFlow;
    import flashx.textLayout.formats.Direction;

    public class SimpleTextField extends Sprite
    {

        private var _width:Number;
        private var _height:Number;
        private var _isArabic:Boolean;
        private var _textFlow:TextFlow;
        private var _containerController:ContainerController;

        protected var _textXML:XML;

        public function SimpleTextField(xml:XML)
        {
            _textXML = xml;
            _containerController = new ContainerController(this);
            buildTextFlow();
        }

        protected function buildTextFlow():void
        {
            if(_textFlow) _textFlow.flowComposer.removeAllControllers();
            _textFlow = null;

            var markup:String = "<TextFlow xmlns='http://ns.adobe.com/textLayout/2008'>"+_textXML.p.toXMLString()+"</TextFlow>";

            _textFlow = TextConverter.importToFlow(markup, TextConverter.TEXT_LAYOUT_FORMAT);
            _textFlow.fontLookup = FontLookup.EMBEDDED_CFF;
            _textFlow.renderingMode = RenderingMode.CFF;

            if(parseFloat(_textXML.@width)) _width = parseFloat(_textXML.@width);
            if(parseFloat(_textXML.@heigt)) _height = parseFloat(_textXML.@height);
            if(_textXML.name() == "arabic")
            {
                _isArabic = true;
                _textFlow.direction = Direction.RTL;
            }
            _containerController.setCompositionSize( _width ? _width : NaN, _height ? _height : NaN);

            _textFlow.flowComposer.addController(_containerController);
            _textFlow.flowComposer.updateAllControllers();

            if(parseFloat(_textXML.@x)) x = parseFloat(_textXML.@x);
            if(parseFloat(_textXML.@y)) y = parseFloat(_textXML.@y);
        }

        override public function get x():Number
        {
            if(_isArabic) return super.x + width;
            else return super.x;
        }

        override public function set x(value:Number):void
        {
            if(_isArabic) super.x = value - width;
            else super.x = value;
        }

        override public function get width():Number
        {
            if(_width) return _width;
            else return super.width;
        }

        override public function get height():Number
        {
            if(_height) return _height;
            else return super.height;
        }
    }
}
