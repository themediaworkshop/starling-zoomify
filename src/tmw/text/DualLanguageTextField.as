/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 24/10/2013
 * Time: 15:37
 * The Media Workshop Ltd
 */
package tmw.text
{
    import flash.display.Sprite;

    public class DualLanguageTextField extends Sprite
    {

        private var _english:SimpleTextField;
        private var _arabic:SimpleTextField;
        private var _active:SimpleTextField;

        public function DualLanguageTextField(xml:XML)
        {
            if(!xml.english[0] || !xml.arabic[0]) throw new Error("Malformed XML. DualLanguageTextField expects <english> and <arabic> nodes: "+xml);

            _english = new SimpleTextField(xml.english[0]);
            _arabic = new SimpleTextField(xml.arabic[0]);
            _active = _english;

        }

        public function showEnglish():void
        {
            if(_arabic.parent) removeChild(_arabic);
            addChild(_english);
            _active = _english;
        }

        public function showArabic():void
        {
            if(_english.parent) removeChild(_english);
            addChild(_arabic);
            _active = _arabic;
        }

        override public function get width():Number
        {
            return _active.width;
        }

        override public function get height():Number
        {
            return _active.height;
        }
    }
}
