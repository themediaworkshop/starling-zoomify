/**
 * Created with IntelliJ IDEA.
 * User: darrencperry
 * Date: 21/03/2014
 * Time: 10:33
 * The Media Workshop Ltd
 */
package
{
    import flash.display.DisplayObjectContainer;

    import org.robotlegs.base.ContextEvent;

    import tmw.arabianmaps.controller.bootstraps.BootstrapControllers;
    import tmw.arabianmaps.controller.bootstraps.BootstrapMediators;

    import tmw.arabianmaps.controller.commands.StartupCommand;
    import tmw.arabianmaps.model.ITileModel;
    import tmw.arabianmaps.model.TileModel;

    import tmw.arabianmaps.service.TileLoader;
    import tmw.arabianmaps.service.ITileLoader;

    import MainView;
    import MainViewMediator;

    import tmw.exhibit.ExhibitContext;

    public class StarlingZoomifyContext extends ExhibitContext
    {
        public function StarlingZoomifyContext(contextView:DisplayObjectContainer)
        {
            super(contextView);
        }

        override protected function addMainViews():void
        {
            // Add exhibit specific bootstraps here
            // Remember to map view mediators for each window they will be added to

            new BootstrapMediators(_windowsModel.windows[0].mediatorMap);
            _windowsModel.windows[0].mediatorMap.mapView(MainView, MainViewMediator);

            var mainView:MainView = new MainView();
            _windowsModel.windows[0].rootView.addChild(mainView);
        }

        override public function startup():void
        {
            new BootstrapControllers(_injector,_commandMap);
            super.startup();
        }
    }
}
